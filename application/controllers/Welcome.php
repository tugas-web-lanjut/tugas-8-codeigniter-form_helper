<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		date_default_timezone_set('Asia/Jakarta');
		$this->load->view('index', null, FALSE);
	}

	public function result()
	{
		$tgl_input = $this->input->post('waktu_input');
		$nama_lengkap = $this->input->post('nama_lengkap');
		$nim = $this->input->post('nim');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$hobi = $this->input->post('hobi');
		$kelas = $this->input->post('kelas');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$password = $this->input->post('password');
		$alamat = $this->input->post('alamat');


		// Validation begin
		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required',
			array('required' => 'Nama harus diisi.')
		);

		$this->form_validation->set_rules('nim', 'NIM', 'required',
			array('required' => 'NIM harus diisi.')
		);

		$this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required',
			array('required' => 'Tanggal Lahir harus diisi.')
		);

		$this->form_validation->set_rules('password', 'Password', 'required',
			array('required' => 'Password harus diisi.')
		);
		$this->form_validation->set_rules('konfirmasi_password', 'Konfirmasi Password', 'required|matches[password]',
			array(
				'required' => 'Konfirmasi Password harus diisi.',
				'matches' => 'Password tidak sama.',
			)
		);

		$this->form_validation->set_rules('alamat', 'Alamat', 'required',
			array('required' => 'Alamat harus diisi.')
		);

		if ($this->form_validation->run() == FALSE)
		{
			self::index();
		}
		else
		{
			$data = [
				'tgl_input' => $tgl_input,
				'nama_lengkap' => $nama_lengkap,
				'nim' => $nim,
				'jenis_kelamin' => $jenis_kelamin,
				'hobi' => implode("," , $hobi),
				'kelas' => $kelas,
				'tgl_lahir' => $tgl_lahir,
				'password' => $password,
				'encrypted_password' => md5($password),
				'alamat' => $alamat
			];

			$this->load->view('result', $data, FALSE);
		}
	}
}
