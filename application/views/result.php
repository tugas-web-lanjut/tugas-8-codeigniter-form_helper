<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Mahasiswa</title>
</head>
<body>

    <h2>Data Mahasiswa</h2>

    <p>Nama : <?php echo $nama_lengkap; ?></p>
    <p>NIM : <?php echo $nim; ?></p>
    <p>Jenis Kelamin : <?php echo $jenis_kelamin; ?></p>
    <p>Hobi : <?php echo $hobi; ?></p>
    <p>Kelas : <?php echo $kelas; ?></p>
    <p>Tanggal Lahir : <?php echo $tgl_lahir; ?></p>
    <p>Password : <?php echo $password; ?></p>
    <p>Password  Terenkripsi: <?php echo $encrypted_password; ?></p>
    <p>Alamat : <?php echo $alamat; ?></p>
    <p>Tanggal Penginputan : <?php echo $tgl_input; ?></p>

</body>
</html>